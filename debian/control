Source: nats.c
Priority: optional
Maintainer: Victor Seva <vseva@debian.org>
Build-Depends:
 cmake (>=3.13),
 debhelper-compat (= 12),
 libprotobuf-c-dev,
 libsodium-dev,
 libssl-dev,
Standards-Version: 4.6.1
Section: libs
Homepage: https://github.com/nats-io/nats.c/
Vcs-Browser: https://salsa.debian.org/debian/nats.c
Vcs-Git: https://salsa.debian.org/debian/nats.c.git

Package: libnats-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends:
 libnats3.3 (= ${binary:Version}),
 ${misc:Depends},
Description: C client for the NATS messaging system (development files)
 NATS messaging enables the exchange of data that is segmented into messages
 among computer applications and services. These messages are addressed by
 subjects and do not depend on network location. This provides an abstraction
 layer between the application or service and the underlying physical network.
 Data is encoded and framed as a message and sent by a publisher.
 The message is received, decoded, and processed by one or more subscribers.
 .
 This package provides the C headers for NATS

Package: libnats3.3
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: C client for the NATS messaging system
 NATS messaging enables the exchange of data that is segmented into messages
 among computer applications and services. These messages are addressed by
 subjects and do not depend on network location. This provides an abstraction
 layer between the application or service and the underlying physical network.
 Data is encoded and framed as a message and sent by a publisher.
 The message is received, decoded, and processed by one or more subscribers.
 .
 This package provides the C shared libraries for NATS
